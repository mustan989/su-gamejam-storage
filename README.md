# SU GameJam Storage

## Description

Storage for SU GameJam based on file system

## Usage:

0. Build (if necessary):

   `./cmd/build.sh <os>` - os can be `linux` or `windows`. For more detailed info read [documentation]()
1. Prepare env:
    1. `CONFIG_PATH` - full or relative path to config as shown in example [file](config/config.yaml). Default value
       is `config/config.yaml`
    2. `DEBUG=<on/off>` - turns static page for testing file upload. Default is `off`
2. Run app:

   `./app` or `./app.exe`

## Road Map

TODOS:

- logging
- authorization/authentication
- duplicate check
- refactoring:
    - project structure
    - optimise request/response structures
    - replace struct usages to interfaces
    - ???