#!/bin/bash

if [ $# -eq 0 ]; then
  echo "No arguments supplied"
  exit 1
fi

env GOOS="$1" go build -o ../bin/app-"$1"
