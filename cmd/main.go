package main

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"gitlab.com/mustan989/su-gamejam-storage/internal/config"
	"gitlab.com/mustan989/su-gamejam-storage/internal/handler"
	"gitlab.com/mustan989/su-gamejam-storage/internal/service"
	"gitlab.com/mustan989/su-gamejam-storage/internal/storage"
	"log"
	"os"
)

func main() {
	log.Fatalf("app stopped with error: %s", run())
}

const defaultCfgPath = "config/config.yaml"

func run() error {
	cfgPath := os.Getenv("CONFIG_PATH")
	if len(cfgPath) == 0 {
		cfgPath = defaultCfgPath
	}
	cfg, err := config.ParseFile(cfgPath, config.YAML)
	if err != nil {
		return err
	}

	s := storage.NewFileSystem(storage.WithConfig(cfg.Storage))
	if err := s.Init(); err != nil {
		return err
	}
	svc := service.NewStorage(s)
	h := handler.NewHandler(svc)

	app := echo.New()

	h.AddRoutes(app)

	return app.Start(fmt.Sprintf(":%d", cfg.Server.Port))
}
