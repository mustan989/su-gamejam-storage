package config

import (
	"encoding/json"
	"encoding/xml"
	"errors"
	"gopkg.in/yaml.v3"
	"os"
)

type Config struct {
	Storage *Storage `json:"storage" yaml:"storage"`
	Server  *Server  `json:"server" yaml:"server"`
}

type Storage struct {
	Root string `json:"root" yaml:"root"`
}

type Server struct {
	Port int `json:"port" yaml:"port"`
}

func New() *Config {
	return &Config{
		Storage: &Storage{},
		Server:  &Server{},
	}
}

func ParseFile(path string, format int) (*Config, error) {
	config := New()
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	switch format {
	case YAML:
		if err := yaml.NewDecoder(file).Decode(config); err != nil {
			return nil, err
		}
	case JSON:
		if err := json.NewDecoder(file).Decode(config); err != nil {
			return nil, err
		}
	case XML:
		if err := xml.NewDecoder(file).Decode(config); err != nil {
			return nil, err
		}
	default:
		return nil, errors.New("unknown format")
	}
	return config, nil
}

const (
	YAML = iota
	JSON
	XML
)

var formatString = map[int]string{
	YAML: "yaml",
	JSON: "json",
	XML:  "xml",
}

func FormatString(format int) string {
	return formatString[format]
}
