package handler

import (
	"errors"
	"fmt"
	"github.com/labstack/echo/v4"
	"gitlab.com/mustan989/su-gamejam-storage/internal/model"
	"gitlab.com/mustan989/su-gamejam-storage/internal/service"
	"gitlab.com/mustan989/su-gamejam-storage/internal/util"
	"io"
	"net/http"
	"os"
	"strings"
)

type Handler struct {
	svc *service.Storage
}

func (h *Handler) AddApplicationForm(c echo.Context) error {
	files := make([]*model.File, 0)
	types := model.AllFileTypes()

	for _, ft := range types {
		fileName := ft.String()
		formFile, err := c.FormFile(fileName)
		if err != nil {
			return c.JSON(http.StatusBadRequest, errMessage("%s must not be empty", fileName))
		}
		file, err := formFile.Open()
		if err != nil {
			return c.JSON(http.StatusBadRequest, errMessage("error getting %s file: %s", fileName))
		}
		bytes, err := io.ReadAll(file)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, errMessage("error reading %s file: %s", fileName, err.Error()))
		}
		files = append(files, &model.File{
			Name:    formFile.Filename,
			Type:    ft,
			Content: bytes,
		})
	}

	teamName := c.FormValue("team_name")
	if len(teamName) == 0 {
		return c.JSON(http.StatusBadRequest, errMessage("team name must not be empty"))
	}

	id, err := h.svc.AddApplication(c.Request().Context(), &model.Application{
		TeamName: teamName,
		Files:    files,
	})
	if err != nil {
		return c.JSON(http.StatusInternalServerError, errMessage("error saving application: %s", err.Error()))
	}
	application, err := h.svc.GetApplication(c.Request().Context(), &model.ApplicationRequest{
		TeamName: teamName,
		ID:       id,
	})
	if err != nil {
		return c.JSON(http.StatusInternalServerError, errMessage("error getting saved data for %s team with %s id: %s", teamName, id, err.Error()))
	}
	return c.JSON(http.StatusCreated, application)
}

func (h *Handler) GetApplications(c echo.Context) error {
	applications, err := h.svc.GetAllApplications(c.Request().Context())
	if err != nil {
		return c.JSON(http.StatusInternalServerError, errMessage("error getting all applications: %s", err.Error()))
	}
	return c.JSON(http.StatusOK, applications)
}

func (h *Handler) GetApplicationsByTeamName(c echo.Context) error {
	teamName := c.Param("team")
	teamResponse, err := h.svc.GetApplicationsByTeamName(c.Request().Context(), &model.TeamRequest{Name: teamName})
	if err != nil {
		return c.JSON(http.StatusInternalServerError, errMessage("error getting applications for %s team: %s", teamName, err.Error()))
	}
	return c.JSON(http.StatusOK, teamResponse)
}

func (h *Handler) GetApplicationByID(c echo.Context) error {
	teamName, applicationID := c.Param("team"), c.Param("application")
	application, err := h.svc.GetApplication(c.Request().Context(), &model.ApplicationRequest{
		TeamName: teamName,
		ID:       applicationID,
	})
	if err != nil {
		return c.JSON(http.StatusInternalServerError, errMessage("error getting application for %s team with %s id: %s", teamName, applicationID, err.Error()))
	}
	return c.JSON(http.StatusOK, application)
}

func (h *Handler) GetFile(c echo.Context) error {
	file, err := h.svc.GetFileInfo(c.Request().Context(), &model.FileInfoRequest{
		TeamName:      c.Param("team"),
		ApplicationID: c.Param("application"),
		FileType:      c.Param("type"),
	})
	if err != nil {
		if errors.Is(err, util.ErrNotFound) {
			return c.JSON(http.StatusNotFound, errMessage(err.Error()))
		}
		return c.JSON(http.StatusInternalServerError, errMessage("error getting file: %s", err.Error()))
	}
	return c.Attachment(file.FullPath, file.Name)
}

func NewHandler(service *service.Storage, opts ...Option) *Handler {
	h := newHandler(service)
	for _, opt := range opts {
		opt(h)
	}
	return h
}

func (h *Handler) AddRoutes(app *echo.Echo) {
	app.GET("/applications", h.GetApplications)
	app.POST("/applications", h.AddApplicationForm)
	app.GET("/applications/:team", h.GetApplicationsByTeamName)
	app.GET("/applications/:team/:application", h.GetApplicationByID)
	app.GET("/applications/:team/:application/:type", h.GetFile)

	if debug := strings.ToLower(os.Getenv("DEBUG")); debug == "on" {
		app.Static("/", "static/index.html")
	}
}

func newHandler(s *service.Storage) *Handler {
	return &Handler{svc: s}
}

func errMessage(format string, args ...interface{}) err {
	return err{Msg: fmt.Sprintf(format, args...)}
}

type err struct {
	Msg string `json:"msg"`
}
