package model

type Application struct {
	TeamName string
	ID       string
	Files    []*File
}

type ApplicationRequest struct {
	TeamName string `json:"team_name"`
	ID       string `json:"id"`
}

type ApplicationResponse struct {
	ID    string              `json:"id"`
	Files []*FileInfoResponse `json:"files"`
}

type TeamRequest struct {
	Name string `json:"name"`
}

type TeamResponse struct {
	Name         string                 `json:"name"`
	Applications []*ApplicationResponse `json:"applications"`
}
