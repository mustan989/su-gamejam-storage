package model

type File struct {
	Name     string
	FullPath string
	Type     FileType
	Content  []byte
}

type FileRequest struct {
	TeamName      string `json:"team_name"`
	ApplicationID string `json:"application_id"`
	FileType      string `json:"file_type"`
}

type FileResponse []byte

type FileInfoRequest FileRequest

type FileInfoResponse struct {
	Name     string `json:"name"`
	Type     string `json:"type"`
	FullPath string `json:"-"`
}

type FileType int

func (t FileType) String() string {
	return FileTypeString(t)
}

const (
	Binary = FileType(iota)
	Source
	Documentation
)

func AllFileTypes() []FileType {
	return []FileType{Binary, Source, Documentation}
}

func FileTypeString(ft FileType) string {
	return fileTypeString[ft]
}

func StringFileType(str string) FileType {
	return stringFileType[str]
}

var (
	fileTypeString = map[FileType]string{
		Binary:        "binary",
		Source:        "source",
		Documentation: "documentation",
	}
	stringFileType = map[string]FileType{
		"binary":        Binary,
		"source":        Source,
		"documentation": Documentation,
	}
)
