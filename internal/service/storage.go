package service

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/mustan989/su-gamejam-storage/internal/model"
	"gitlab.com/mustan989/su-gamejam-storage/internal/storage"
	"gitlab.com/mustan989/su-gamejam-storage/internal/util"
)

type Storage struct {
	fs *storage.FileSystem
}

func (s *Storage) AddApplication(ctx context.Context, application *model.Application) (string, error) {
	var id string
	applications, err := s.fs.GetApplicationsByTeamName(application.TeamName)
	if err != nil {
		if !errors.Is(err, util.ErrNotFound) {
			return "", err
		}
		id = "1"
	} else {
		id = fmt.Sprintf("%d", len(applications)+1)
	}
	for _, file := range application.Files {
		if err := s.fs.AddFile(application.TeamName, id, file.Name, file.Type, file.Content); err != nil {
			return "", err
		}
	}
	return id, nil
}

func (s *Storage) GetFile(ctx context.Context, request *model.FileRequest) (model.FileResponse, error) {
	file, err := s.fs.GetFile(request.TeamName, request.ApplicationID, model.StringFileType(request.FileType))
	if err != nil {
		return nil, err
	}
	return file.Content, nil
}

func (s *Storage) GetFileInfo(ctx context.Context, request *model.FileInfoRequest) (*model.FileInfoResponse, error) {
	file, err := s.fs.GetFileInfo(request.TeamName, request.ApplicationID, model.StringFileType(request.FileType))
	if err != nil {
		return nil, err
	}
	return &model.FileInfoResponse{
		Name:     file.Name,
		Type:     file.Type.String(),
		FullPath: file.FullPath,
	}, nil
}

func (s *Storage) GetApplication(ctx context.Context, request *model.ApplicationRequest) (*model.ApplicationResponse, error) {
	application, err := s.fs.GetApplicationByID(request.TeamName, request.ID)
	if err != nil {
		return nil, err
	}
	files := make([]*model.FileInfoResponse, 0)
	for _, file := range application.Files {
		files = append(files, &model.FileInfoResponse{
			Name: file.Name,
			Type: file.Type.String(),
		})
	}
	return &model.ApplicationResponse{ID: application.ID, Files: files}, nil
}

func (s *Storage) GetApplicationsByTeamName(ctx context.Context, request *model.TeamRequest) (*model.TeamResponse, error) {
	apps, err := s.fs.GetApplicationsByTeamName(request.Name)
	if err != nil {
		return nil, err
	}
	applications := make([]*model.ApplicationResponse, 0)
	for _, app := range apps {
		files := make([]*model.FileInfoResponse, 0)
		for _, file := range app.Files {
			files = append(files, &model.FileInfoResponse{
				Name: file.Name,
				Type: file.Type.String(),
			})
		}
		applications = append(applications, &model.ApplicationResponse{ID: app.ID, Files: files})
	}
	return &model.TeamResponse{
		Name:         request.Name,
		Applications: applications,
	}, nil
}

func (s *Storage) GetAllApplications(ctx context.Context) ([]*model.TeamResponse, error) {
	teamsNames, err := s.fs.GetAllTeamsNames()
	if err != nil {
		return nil, err
	}
	teams := make([]*model.TeamResponse, 0)
	for _, name := range teamsNames {
		apps, err := s.fs.GetApplicationsByTeamName(name)
		if err != nil {
			return nil, err
		}
		applications := make([]*model.ApplicationResponse, 0)
		for _, app := range apps {
			files := make([]*model.FileInfoResponse, 0)
			for _, file := range app.Files {
				files = append(files, &model.FileInfoResponse{
					Name: file.Name,
					Type: file.Type.String(),
				})
			}
			applications = append(applications, &model.ApplicationResponse{Files: files})
		}
	}
	return teams, nil
}

func NewStorage(fs *storage.FileSystem, opts ...Option) *Storage {
	s := newStorage(fs)
	for _, opt := range opts {
		opt(s)
	}
	return s
}

func newStorage(fs *storage.FileSystem) *Storage {
	return &Storage{fs: fs}
}
