package storage

import (
	"errors"
	"fmt"
	"gitlab.com/mustan989/su-gamejam-storage/internal/model"
	"gitlab.com/mustan989/su-gamejam-storage/internal/util"
	"os"
)

type FileSystem struct {
	root string
}

func (s *FileSystem) GetFileInfo(teamName, applicationID string, ft model.FileType) (*model.File, error) {
	basePath := fmt.Sprintf("%s/%s/%s/%s", s.root, teamName, applicationID, ft)
	files, err := util.ReadDir(basePath)
	if err != nil {
		return nil, err
	}

	for _, entry := range files {
		if !entry.IsDir() {
			path := fmt.Sprintf("%s/%s", basePath, entry.Name())
			if err != nil {
				return nil, err
			}
			return &model.File{
				Name:     entry.Name(),
				FullPath: path,
				Type:     ft,
				//Content:  bytes,
			}, nil
		}
	}

	return nil, util.ErrNotFound
}

func (s *FileSystem) GetFile(teamName, applicationID string, ft model.FileType) (*model.File, error) {
	basePath := fmt.Sprintf("%s/%s/%s/%s", s.root, teamName, applicationID, ft)
	files, err := util.ReadDir(basePath)
	if err != nil {
		return nil, err
	}

	for _, entry := range files {
		if !entry.IsDir() {
			path := fmt.Sprintf("%s/%s", basePath, entry.Name())
			bytes, err := util.ReadFile(path)
			if err != nil {
				return nil, err
			}
			return &model.File{
				Name:     entry.Name(),
				FullPath: path,
				Type:     ft,
				Content:  bytes,
			}, nil
		}
	}

	return nil, util.ErrNotFound
}

func (s *FileSystem) GetAllFiles(teamName, applicationID string) ([]*model.File, error) {
	files := make([]*model.File, 0)

	types := model.AllFileTypes()
	for _, ft := range types {
		file, err := s.GetFileInfo(teamName, applicationID, ft)
		if err != nil {
			if errors.Is(err, util.ErrNotFound) {
				continue
			}
			return nil, err
		}
		files = append(files, file)
	}

	return files, nil
}

func (s *FileSystem) GetApplicationByID(teamName, applicationID string) (*model.Application, error) {
	files, err := s.GetAllFiles(teamName, applicationID)
	if err != nil {
		return nil, err
	}
	return &model.Application{
		TeamName: teamName,
		ID:       applicationID,
		Files:    files,
	}, nil
}

func (s *FileSystem) GetApplicationsByTeamName(teamName string) ([]*model.Application, error) {
	path := fmt.Sprintf("%s/%s", s.root, teamName)

	applicationDirs, err := util.ReadDir(path)
	if err != nil {
		return nil, err
	}

	applications := make([]*model.Application, 0)

	for _, dir := range applicationDirs {
		application, err := s.GetApplicationByID(teamName, dir.Name())
		if err != nil {
			if errors.Is(err, util.ErrFileIsNotDirectory) {
				continue
			}
			return nil, err
		}
		applications = append(applications, application)
	}

	return applications, nil
}

func (s *FileSystem) GetAllApplications() ([]*model.Application, error) {
	path := s.root

	teamDirs, err := util.ReadDir(path)
	if err != nil {
		return nil, err
	}

	applications := make([]*model.Application, 0)

	for _, dir := range teamDirs {
		subApplications, err := s.GetApplicationsByTeamName(dir.Name())
		if err != nil {
			return nil, err
		}
		applications = append(applications, subApplications...)
	}
	return applications, nil
}

func (s *FileSystem) AddFile(teamName, applicationID, fileName string, ft model.FileType, content []byte) error {
	path := fmt.Sprintf("%s/%s/%s/%s", s.root, teamName, applicationID, ft)
	if err := os.MkdirAll(path, os.ModePerm); err != nil {
		return err
	}
	filePath := fmt.Sprintf("%s/%s", path, fileName)
	dst, err := os.Create(filePath)
	if err != nil {
		return err
	}
	defer util.CloseResource("file", dst)
	if _, err = dst.Write(content); err != nil {
		return err
	}
	return nil
}

func (s *FileSystem) GetAllTeamsNames() ([]string, error) {
	path := s.root

	teamDirs, err := util.ReadDir(path)
	if err != nil {
		return nil, err
	}

	names := make([]string, 0)
	for _, dir := range teamDirs {
		if dir.IsDir() {
			names = append(names, dir.Name())
		}
	}
	return names, nil
}

func NewFileSystem(opts ...Option) *FileSystem {
	fs := defaultFS()
	for _, opt := range opts {
		opt(fs)
	}
	return fs
}

func (s *FileSystem) Init() error {
	err := os.MkdirAll(s.root, os.ModePerm)
	if err != nil {
		return err
	}
	return nil
}

func defaultFS() *FileSystem {
	return &FileSystem{root: "storage"}
}
