package storage

import "gitlab.com/mustan989/su-gamejam-storage/internal/config"

type Option func(fs *FileSystem)

func WithConfig(config *config.Storage) Option {
	return func(fs *FileSystem) {
		fs.root = config.Root
	}
}

func WithRoot(root string) Option {
	return func(fs *FileSystem) {
		fs.root = root
	}
}
