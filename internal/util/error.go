package util

import "errors"

var (
	ErrFileIsNotDirectory = errors.New("file is not directory")
	ErrNotFound           = errors.New("resource not found")
)
