package util

import (
	"errors"
	"io"
	"log"
	"os"
)

func ReadDir(path string) ([]os.DirEntry, error) {
	dir, err := os.Open(path)
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			return nil, ErrNotFound
		}
		return nil, err
	}
	defer CloseResource("directory", dir)
	stat, err := dir.Stat()
	if err != nil {
		return nil, err
	}
	if !stat.IsDir() {
		return nil, ErrFileIsNotDirectory
	}
	return dir.ReadDir(0)
}

func CloseResource(name string, resource io.Closer) {
	if err := resource.Close(); err != nil {
		log.Printf("error closing %s: %s", name, err.Error())
	}
}

func ReadFile(path string) ([]byte, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer CloseResource("file", file)
	return io.ReadAll(file)
}
